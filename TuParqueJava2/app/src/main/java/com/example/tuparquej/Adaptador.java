package com.example.tuparquej;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class Adaptador extends RecyclerView.Adapter<EntidadHolder> {
    private Context context;
    private ArrayList<Entidad> listItems;
    private int itemResource;

    public Adaptador(Context context, ArrayList<Entidad> listItems, int itemResource) {
        this.context = context;
        this.listItems = listItems;
        this.itemResource=itemResource;
    }


    @NonNull
    @Override
    public EntidadHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(this.itemResource,parent,false);
        return new EntidadHolder(this.context, view);
    }

    @Override
    public void onBindViewHolder(@NonNull EntidadHolder holder, int position) {
        Entidad entidad=this.listItems.get(position);
        holder.bindEntidad(entidad);
    }

    @Override
    public int getItemCount() {
        return this.listItems.size();
    }
}
