package com.example.tuparquej;
import android.content.Context;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;


public class EntidadHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    ImageButton btnFoto;
    TextView nombre;
    TextView barrio;
    ImageView estrellas;
    TextView distancia;


    private Entidad entidad;
    private Context context;


    public EntidadHolder(Context context, View itemView) {
        super(itemView);

        this.context = context;

        this.btnFoto= (ImageButton)itemView.findViewById(R.id.buttonParque);
        this.nombre=itemView.findViewById(R.id.textViewNombre);
        this.barrio=itemView.findViewById(R.id.textViewBarrio);
        this.estrellas=itemView.findViewById(R.id.imageViewEstrellas);
        this.distancia=itemView.findViewById(R.id.textViewDistancia);

        itemView.setOnClickListener(this);
    }
    public void bindEntidad(Entidad entidad){
        this.entidad=entidad;
        this.nombre.setText(entidad.getNombre());
        this.barrio.setText(entidad.getBarrio());

        if(entidad.getImagen()!=null)
        {
            btnFoto.setBackground(null);
            Picasso.get().load(entidad.getImagen()).fit().into(btnFoto);
        }


        double e=entidad.getEstrellas();
        if(e==0)
        {
            estrellas.setImageResource(R.drawable.eceroestrellas);
        }
        else if(e<=1)
        {
            estrellas.setImageResource(R.drawable.eunaestrella);
        }
        else if(e<=2)
        {
            estrellas.setImageResource(R.drawable.edosestrellas);
        }
        else if(e<=3)
        {
            estrellas.setImageResource(R.drawable.etresestrellas);
        }
        else if(e<=4)
        {
            estrellas.setImageResource(R.drawable.ecuatroestrellas);
        }
        else{
            estrellas.setImageResource(R.drawable.ecincoestrellas);
        }

        distancia.setText(calcularDistancia()+" metros");

    }
    @Override
    public void onClick(View v)
    {
        if(this.entidad!=null)
        {
            Toast.makeText(context, "Se ha seleccionado: "+ this.entidad.getNombre(), Toast.LENGTH_SHORT).show();
        }
    }
    private String calcularDistancia(){

        return distance(MainActivity.latitude,entidad.getLatitud(), MainActivity.longitude, entidad.getLongitud())+"";
    }
    public static int distance(double lat1, double lat2, double lon1,
                               double lon2) {
        if(lat1==0)
        {
            lat1=4.602834;
            lon1=-74.064783;
        }
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        float dist = (float) (earthRadius * c);

        return (int) dist;

    }
}
